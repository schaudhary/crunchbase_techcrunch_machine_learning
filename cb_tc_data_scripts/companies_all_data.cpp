//Build using g++ companies_all_data.cpp -ljson_linux-gcc-4.7_libmt
#include <iostream>
#include <memory>
#include "/usr/local/include/json/json.h"
#include "/usr/local/include/json/value.h"
#include "/usr/local/include/json/reader.h"
#include "/usr/local/include/json/writer.h"
#include "/usr/local/include/json/forwards.h"
#include "/usr/local/include/json/features.h"
#include "/usr/local/include/json/config.h"
#include "/usr/local/include/json/autolink.h"
#include <fstream>
using namespace std;

int main() {
	Json::Value root;   // will contains the root value after parsing.
	Json::Reader reader;
	ifstream inf;
	ofstream of;
	inf.open("list_companies.txt");
	of.open("list_companies_out.txt");

	bool parsingSuccessful = reader.parse(inf, root );
	if ( !parsingSuccessful )	{
    		// report to the user the failure and their locations in the document.
    		std::cout  << "Failed to parse configuration\n";
              	//<< reader.getFormattedErrorMessages();
	}	

	//cout<<"....."<<root.toStyledString()<<endl;
	cout<<"size = "<<root.size()<<endl;
	//cout<<root["founded_day"]<<endl;

	for(int i = 0; i < (int)root.size(); ++i) {
		of<<root[i]["name"]<<root[i]["permalink"]<<endl;
	}

	inf.close();
	of.close();
	return 0;
	
}
