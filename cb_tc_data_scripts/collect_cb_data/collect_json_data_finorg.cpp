#include<iostream>
#include<string>
#include<string.h>
#include<map>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<stdio.h>
using namespace std;

#define LIMIT 5

int main() {
	ifstream inf;
	inf.open("list_financial-organizations_out.txt");
	
	string company_name;
	string company_link, line;

	string system_cmd;
	char cmd[1024];

	int cnt = 0;
	while(inf.good()) {
		getline(inf, company_name);
		getline(inf, company_link);
		getline(inf, line);
		system_cmd = "";

		/* Remove quotes from name and link */
		if(company_name == "" || company_link == "") break;
		company_name = company_name.substr(1, 
						company_name.size() - 2);
		company_link = company_link.substr(1,
                                                company_link.size() - 2);


		/* Conctruct the wget command to download the company data */
		system_cmd += "wget http://api.crunchbase.com/v/1/financial-organization/";
		system_cmd += company_link;
		system_cmd += ".js?api_key=wg4p8s4qptdzsrjcwagh2j6b -O ";
		system_cmd += company_link;
		system_cmd += "_finorg.json";
		
		strcpy(cmd, system_cmd.c_str());
		system(cmd);

		/* Handle if there is some constraint on data collection rate */
		//sleep(1);
		//if(cnt > LIMIT) break;
		//++cnt;
	}

	inf.close();
	return (0);
}
