//Build using g++ process_json_data_companies.cpp -ljson_linux-gcc-4.7_libmt
#include<iostream>
#include<string>
#include<string.h>
#include<map>
#include<vector>
#include<fstream>
#include<cstdlib>
#include<stdio.h>
#include "/usr/local/include/json/json.h"
#include "/usr/local/include/json/value.h"
#include "/usr/local/include/json/reader.h"
#include "/usr/local/include/json/writer.h"
#include "/usr/local/include/json/forwards.h"
#include "/usr/local/include/json/features.h"
#include "/usr/local/include/json/config.h"
#include "/usr/local/include/json/autolink.h"
using namespace std;

int calculate_age(int year, int month, int day) {
	return 10;
	//Need to implement this function or use some
	//existing API.
}

int main() {
	ifstream inf, inf_json;
	ofstream of;
	inf.open("list_companies_out.txt");
	of.open("companies_all_data.txt");
	
	string company_name;
	string company_link, line;

	string file_name;
	char fname[512];

	Json::Value root;   // will contains the root value after parsing.
        Json::Reader reader;

	int cnt = 0;
	while(inf.good()) {
		getline(inf, company_name);
		getline(inf, company_link);
		getline(inf, line);
		file_name = "";

		/* Remove quotes from name and link */
		if(company_name == "" || company_link == "") break;
		company_name = company_name.substr(1, 
						company_name.size() - 2);
		company_link = company_link.substr(1,
                                                company_link.size() - 2);


		file_name += company_link;
		file_name += "_json.json";
		strcpy(fname, file_name.c_str());

		inf_json.open(fname);

		bool parsingSuccessful = reader.parse(inf_json, root );
        	if ( !parsingSuccessful ) {
                	// report to the user the failure and their locations in the document.
                	std::cout  << "Failed to parse file." << fname << endl;
        	}

		cout<<company_name<<' '<<company_link<<' '<<root["number_of_employees"]<<' '<<root["founded_year"]<<endl;
		//cout<<system_cmd.c_str()<<endl;

		cout<<"milestones# = "<<root["milestones"].size()<<endl;	
		cout<<"offices# = "<<root["offices"].size()<<endl;	
		cout<<"products# = "<<root["products"].size()<<endl<<endl;	
		cout<<"providers# = "<<root["providerships"].size()<<endl<<endl;	
		cout<<"acquisitions# = "<<root["acquisitions"].size()<<endl<<endl;	
		cout<<"competitor# = "<<root["competitions"].size()<<endl<<endl;	
		cout<<"funding_rounds# = "<<root["funding_rounds"].size()<<endl<<endl;	
		cout<<"total_money_raised= "<<root["total_money_raised"].size()<<endl<<endl;	
		cout<<"investments#= "<<root["investments"].size()<<endl<<endl;	

		inf_json.close();
		if(cnt > 10) break;
		++cnt;
	}

	inf.close();
	of.close();
	return 0;
}
