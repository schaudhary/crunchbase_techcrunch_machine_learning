subplot(2, 1,1);
hist(p(1:100, 1))
 title('Histogram of Marginal Probabilities given by BN for Label 0 for FR dataset');
 xlabel('Occurrences');
 ylabel('Marginal Probabilities');
 hold
 subplot(2, 1,2)
 hist(p(1:100, 2))
 title('Histogram of Marginal Probabilities given by BN for Label 1 for FR dataset');
 xlabel('Occurrences');
 ylabel('Marginal Probabilities');