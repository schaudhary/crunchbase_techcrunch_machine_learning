load('final_data_acq_train.txt');
a1 = final_data_acq_train;
b1 = a1(:, 2:27);
c1 = a1(:, 28);
op = statset('Display', 'iter');
res1 = svmtrain(b1, c1, 'method', 'SMO', 'kernel_function', 'mlp','options', op);

load('final_data_acq_cross.txt');
a2 = final_data_acq_cross;
b2 = a2(:, 2:27);

b21 = b2(1:4000, 1:26);
b22 = b2(4001:8000, 1:26);
b23 = b2(8001:10000, 1:26);

ans1 = svmclassify(res1,b21);
ans2 = svmclassify(res1,b22);
ans3 = svmclassify(res1,b23);

no_corr = 0;
no_incorr = 0;

for i = 1:4000
	if (ans1(i) == a2(i, 28))
	no_corr = no_corr + 1;
	else no_incorr = no_incorr + 1;
	end
end

for i = 1:4000
	if (ans2(i) == a2(i + 4000, 28))
	no_corr = no_corr + 1;
	else no_incorr = no_incorr + 1;
	end
end

for i = 1:2000
	if (ans3(i) == a2(i + 8000, 28))
	no_corr = no_corr + 1;
	else no_incorr = no_incorr + 1;
	end
end

no_corr
no_incorr

%can now compare ans1 with a2(:, 28) and ans2 with a2(:, 28)

%similarly can cross check on the trained data itself
%and can predict on the eval data.

%--------same goes for 'fr' dataset

